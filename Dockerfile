ARG TAG=alpine
FROM node:${TAG}

RUN apk add --no-cache \
        build-base \
        ca-certificates \
        git
