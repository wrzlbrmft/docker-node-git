# docker-node-git

Node.js with Git for CI/CD pipelines in Docker

Based on https://hub.docker.com/_/node . Just adding Git.

```
docker pull wrzlbrmft/node-git:<version>
```

See also:

  * https://nodejs.org/
  * https://git-scm.com/
  * https://hub.docker.com/r/wrzlbrmft/node-git/tags

## License

The content of this repository is distributed under the terms of the
[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).
